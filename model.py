# -*- coding: utf-8 -*-
"""
@author: Awais
@descrption: combine all the files to make a final model
    
"""

import pandas as pd
import numpy as np
from predict import predict_random,predict_gallup, predict_dunya,predict_partyHistory,predict_districtHistory,\
    predict_twitter, predict_capi
from preprocessing import NA_list_preprocessed
from ml import bo_parameter_serach
from utils import results_to_party
from tqdm import tqdm
import time
import json


#==============================================================================
# Combines predictions of different data sources to give a final prediction of a
# candidate's win
#==============================================================================
def final_model(paras):
    para0,para1,para2,para3,para4,para5,para6,para7,para8,para9,para10,para11 = paras
    print("....")
    df_NA_list = NA_list_preprocessed(country = "Morocco")
    constituencies = df_NA_list["Constituency Number (ID)"].unique().tolist()
    # I'm replacing it with 'name_4', maybe it's higher level
    # with open('data/Morocco/maroc-swing.json') as f:
    #     dct = json.load(f)
    # constituencies = [dct['features'][i]['properties']['name_4'] for i in range(0,len(dct['features']))]
    constituencies = np.asarray(constituencies)
 
    serial_number = []#["924","1054","2171","1509","1359","1540","2029","1293","356","1729","2362","1619","1826","2362"]
    rigged_constituencies = []#["NA-213","NA-223","NA-108","NA-256","NA-247","NA-53","NA-95","NA-243","NA-35","NA-132","NA-65","NA-69","NA-78","NA-124"]
    rigged_candidates = []#["Asif Ali Zadari","Makhdoom Jamil uz Zaman","ABID SHER ALI","Muhammad Najeeb Haroo","Arf Ur Rehman Alvi","Imran Ahmed Khan Niazi","IMRAN AHMED KHAN NIAZI","Imran Ahmed Khan Niazi","Imran Ahmad Khan Niazi","Mian Muhammad Shehbaz Sharif","Parvez Elahi","Chaudhary Nisar Ali Khan","Ahsan iqbal chaudhary","Muhammad Hamza Shehbaz Sharif"]
    df_rigged = pd.DataFrame({"constituency":rigged_constituencies,"candidate":rigged_candidates,"serial":serial_number})
    # Results Data Frame
    list_results = []
    file_line = 'C:/Users/antoi/Documents/Programming/electoral-prediction-model-pk/data/Morocco/maroc-swing.json'
    print(constituencies)
    with open(file_line) as f:
        data = json.load(f)
    for constituency in tqdm(constituencies):
        # slice data for one constituency
        is_relevant_constituency = df_NA_list["Constituency Number (ID)"] == constituency
        current_constituency_data = df_NA_list[is_relevant_constituency]
        # predetermined constituncies
        if constituency in rigged_constituencies:

            winning_candidate_name = df_rigged[df_rigged["constituency"]==constituency]["candidate"].tolist()[0]
            
            winning_serial_number = df_rigged[df_rigged["constituency"]==constituency]["serial"].tolist()[0]
            list_results.append([constituency, winning_serial_number, winning_candidate_name])
        else:            
            # predict
            candidate_prob = para0*np.array(predict_dunya(current_constituency_data))
            constituency_name = current_constituency_data['Constituency Name'].iloc[0]
            # result_file_name = ["Gallup_2017_1.csv","Gallup_2017_2.csv","Gallup_2018_1.csv","Gallup_2018_2.csv","IPOR_2018_1.csv"]
            paras = [para1, para2, para3, para4, para5] # associated parameters
            # candidate_prob += para1*np.array(predict_gallup(current_constituency_data, survey_name = result_file_name[0]))
            # candidate_prob += para2*np.array(predict_gallup(current_constituency_data, survey_name = result_file_name[1]))
            # candidate_prob += para3*np.array(predict_gallup(current_constituency_data, survey_name = result_file_name[2]))
            # candidate_prob += para4*np.array(predict_gallup(current_constituency_data, survey_name = result_file_name[3]))
            # candidate_prob += para5*np.array(predict_gallup(current_constituency_data, survey_name = result_file_name[4]))

            # Morocco
            # candidate_prob = para0 * np.array(predict_dunya(current_constituency_data))  # HERE TO CHANGE
            # result_file_name = [] # polls
            # paras = [] # associated parameters
            # candidate_prob = para0 * np.array(predict_capi(current_constituency_data))

            # for result_file, para in zip(result_file_name, paras):
            #     candidate_prob += para*np.array(predict_capi(current_constituency_data, survey_name=result_file))
                # candidate_prob += para * np.array(predict_gallup(current_constituency_data, survey_name=result_file))

            # candidate_prob += para6*np.array(predict_partyHistory(current_constituency_data))
            #
            # result_file_name = ["results_1997.csv","results_2002.csv","results_2008.csv","results_2013.csv"]
            result_file_name = []
            # candidate_prob += para7*np.array(predict_districtHistory(current_constituency_data, file_name=result_file_name[0]))
            # candidate_prob += para8*np.array(predict_districtHistory(current_constituency_data, file_name=result_file_name[1]))
            # candidate_prob += para9*np.array(predict_districtHistory(current_constituency_data, file_name=result_file_name[2]))
            # candidate_prob += para10*np.array(predict_districtHistory(current_constituency_data, file_name=result_file_name[3]))
            # candidate_prob += para11*np.array(predict_twitter(current_constituency_data))

            
            
            # list_candidates = current_constituency_data["Name of candidate"].tolist()
            # winning_candidate_name = list_candidates[np.argsort(candidate_prob)[-1]]
            # winning_candidate = current_constituency_data["Name of candidate"] == winning_candidate_name
            # winning_serial_number = current_constituency_data[winning_candidate]["Serial Number"].tolist()[0]
            # list_results.append([constituency, winning_serial_number, winning_candidate_name])
            list_parties = current_constituency_data["Party Affiliation"].tolist()
            winning_party_name = list_parties[np.argsort(candidate_prob)[-1]]
            print("winning_party_name: ", winning_party_name) # je devrai stocker le parti gagnant et la constituency
            # here we want to put the results in the geojson file matching on the constituency names
            winning_party = current_constituency_data["Party Affiliation"] == winning_party_name
            winning_serial_number = current_constituency_data[winning_party]["Serial Number"].tolist()[0]
            list_results.append([constituency, winning_serial_number, winning_party_name])
            # update geojson
            # find constituency in
            print("constituency_name: ", constituency_name)
            constituency_feature = next((item for item in data['features'] if item['properties']['name_4'] == constituency_name), None)
            print("constituency_feature: ", constituency_feature)
            # constituency_feature['properties']['results'] = winning_party_name
            constituency_feature['properties']['winner'] = winning_party_name
    with open('resultshaha.geojson', 'w') as f:
        json.dump(data, f)

    df_results=pd.DataFrame(list_results,columns=['Constituency','Predicted Winning Serial Number',
    'Predicted Winning Name of Candidate'])
    # save as results to csv
    df_results.to_csv("results/final_result.csv",index=False) 
    seat_wise_result = df_results
    # Saves result in term of party representaion
    seat_wise_result,party_wise_result = results_to_party("results/final_result.csv")
    
    return party_wise_result, seat_wise_result