from geojson import Point, Feature, FeatureCollection, dump
import geopandas as gpd

constituencies = gpd.read_file('maroc-swing.json')
provinces = gpd.read_file('research.geojson')

join = gpd.sjoin(constituencies, provinces, how='inner',
             op='within', lsuffix='left', rsuffix='right')

with open('join.geojson', 'w') as f:
   dump(join, f)