import json
import pandas as pd

i = 0
j = 0
d = []
for feature in dct['features']:
    j+=1
    for key in feature.get("properties", {}).get("results", {}):
         name_1 = feature.get("properties", {}).get("name_1")
         name_4 = feature.get("properties", {}).get("name_4", {})
         i+=1
         d.append({"Party Affiliation": key,
                   "Province": name_1,
                   "Constituency Name": name_4,
                   "Constituency Number (ID)": "MAR - " + str(j),
                   "Serial Number":i})

column_names = ["Serial Number","Province","Name of candidate","Address of the contestant","Symbol Alloted","Party Affiliation","Constituency Number (ID)","Constituency Name"]
df = pd.DataFrame(d, columns = column_names)

df.to_csv("NA_List_maroc2.csv")
