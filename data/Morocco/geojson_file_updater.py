import json
import pandas as pd


def load_research(filename='research.geojson'):
    """loads json data from filename and return a dict of segments
       keyed by province."""

    with open(filename, encoding='utf-8-sig') as f:
        dct_research = json.load(f)

    data = {}

    for feature in dct_research['features']:
        province = feature.get("properties", {}).get("name")
        segments = feature.get("properties", {}).get("segments")
        data[province] = segments

    return data


def main():
    # load data into a dict keyed by province
    research = load_research()

    with open('maroc-swing.json') as f:
        dct_constituencies = json.load(f)

    d = []
    for feature in dct_constituencies['features']:

        province = feature.get("properties", {}).get("name_2", "")
        constituency = feature.get("properties", {}).get("name_4", "")

        # this is now a dict lookup rather than loading a json file
        try:
            segments = research[province]

            for key in feature.get("properties", {}).get("results", {}):
                d.append({"Party Affiliation": key,
                          "Province": province,
                          "Constituency": constituency,
                          "segments": segments})
        except KeyError:
            print(province + " not found")

    column_names = ["Constituency", "segments"]
    df = pd.DataFrame(d, columns=column_names)

    print(df.head())

    df_parties = pd.read_csv('NA_List_maroc2.csv',header = 0, index_col=0)
    parties = [party for party in df_parties['Party Affiliation'].unique()]
    df.drop_duplicates(subset=['Constituency', 'segments'], inplace=True) # necesito otras cosas

    df[parties] = df.apply(parse_segment, axis=1, result_type='expand')
    df.drop(columns=['Province', 'Party Affiliation', 'segments'], inplace=True)
    df = df.rename({"Constituency Name":"Constituency"},axis =1)
    df.to_csv('swing_like_dunya.csv')

import ast


def parse_segment(row):
    segment = row['segments']
    segment = ast.literal_eval(segment)
    # print("segment: ", segment)
    results = []
    for party in parties:
        if party in segment.keys():
            v_i = segment[party]['intention_rate']
            results.append(v_i)
        else:
            v_i = 0
            results.append(v_i)
    # print("results: ", results)
    return results


if __name__ == '__main__':
    main()
    # Load data
