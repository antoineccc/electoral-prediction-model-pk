# Predicting Election using a Novel Rigged Model
<p align="center">
  <img src="https://i.stack.imgur.com/RISRi.jpg" alt="prediction"  />
 
<iframe width="100%" height="520" frameborder="0" src="https://antoinecomp.carto.com/builder/ba829a75-7938-428e-a963-88c5a694bd88/embed" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe>

  ```
  Election results as predicted by this model before elections.
  ```
</p>

## Introduction

This repository contains code for the election prediction model to predict 2020's election of Morocco.

The original model was for Pakistan in 2018 and **won first prize in nation-wide data science competiton**. Paper based on this model is accepted in the special issue of Spriger's journal on Big Data and Politics. Find more about this model [here](https://awaisrauf.github.io/election_prediction).
<p align="center">
  <img src="https://raw.githubusercontent.com/awaisrauf/GE2018/master/imgs/cermony.jpg" alt="cermony"  />
</p>

Our main objective is to predict the winner of each constituency, the model outputs a vector of probabilities of winning the constituency for each candidate for each constituency. For instance, if a constituency has five candidates then output of the model might look like: [0.2,0.32,0.43,0.02,0.03]. Each data source gives one such probability vector for each constituency. We assumed results for certain constituencies based on domain knowledge and employed Bayesian optimization to combine these vectors to have the final result. Following the tradition of election forecasting models, win probability for a particular candidate is a function of three variables; election history, surveys, and popularity based on social media:

.

```math
\overrightarrow{p}_c=f(election history, surveys, social media)
```

We can formulate our model as follows 

```math
\overrightarrow{p}_c=\sum_{j=1}^J\alpha [j]h(j,c)+\sum_{k=1}^K\beta [k]s(k,c)+ \gamma\overrightarrow{t} + \delta\overrightarrow{q}\\
w_c = \arg\max(\overrightarrow{p}_c)
```

where: - $\overrightarrow{p}_c$: probability vector for c-th constituency, where →pc∈Rnc, nc: total number of candidates in c-th constituency.
- J: Total number of past elections used in the model
- K: Total number of surveys used in the model
- $\overrightarrow{α},\overrightarrow{β}, γ, δ$: hyper-parameters where $0≤α[i],β[j],γ,δ≤1$ and $\overrightarrow{α}∈R^J$, $\overrightarrow{β}∈R^K$
- $h(j,c)$: function which returns probability vector for a particular constituency c based on one past election j
- $s(k,c)$: function which returns probability vector for a particular constituency c based on one poll k
- $\overrightarrow{t}$: probability vector from Twitter data
- $\overrightarrow{q}$: overall likelihood of candidates based on all the previous elections
- $w_c$: wining candidate.

In this model, each data source produces a probability vector for each constituency. The process for the computation of this probability vector is explained in next sections. Bayesian optimization is then employed to find optimal values of hyper-parameters such as $\overrightarrow{α},\overrightarrow{β}, γ, δ$ to combine these vectors for a final result. We can talk about the models later on.

# To-Do


## Change

Apart from cleaning we had to: 
- create functions to adapt capi data to the format the model was waiting for
- create a geojson output in predict()


## Dependencies 

* scipy 0.18.1
* matplotlib 2.0.0
* pandas 0.19.2
* tqdm 4.28.1
* numpy 1.11.3
* bayesian_optimization 0.6.0
* thesis 0.0.01


## How to run the code
```
cd GE2018
pip install -r requirements.txt
python main.py
```


## Reference

Please cite this.
```
@article{awais2019leveraging,
  title={Leveraging big data for politics: predicting general election of Pakistan using a novel rigged model},
  author={Awais, Muhammad and Hassan, Saeed-Ul and Ahmed, Ali},
  journal={Journal of Ambient Intelligence and Humanized Computing},
  pages={1--9},
  year={2019},
  publisher={Springer}
}
```

## License

This repository is licensed under the terms of the GNU AGPLv3 license.
